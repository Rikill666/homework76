import React from 'react';
import {Switch, Route} from "react-router-dom";

import Chat from "./containers/Chat/Chat";

const App = () => {
  return (
        <Switch>
          <Route path="/" exact component={Chat}/>
          <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
  );
};

export default App;
