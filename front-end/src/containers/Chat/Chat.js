import React, {Component} from 'react';
import ChatForm from "../../components/ChatForm/ChatForm";
import {Container} from "reactstrap";
import ChatMessages from "../../components/ChatMessages/ChatMessages";
import {connect} from "react-redux";
import {addNewMessage, getThirtyMessages, getThirtyTimeMessages} from "../../store/actions/chatActions";

class Chat extends Component {
    interval = null;
    state = {
        message: "",
        author: ""
    };
    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    addMessage = async (e) => {
        e.preventDefault();
        document.getElementById('myForm').reset();
        const message = {
            author: this.state.author,
            message:this.state.message
        };
        this.props.addNewMessage(message);
        this.setState({message: "", author: ""});
    };
    getMessage = () => {
        if (this.props.lastPostTime) {
            this.props.getThirtyTimeMessages(this.props.lastPostTime);
        } else {
            this.props.getThirtyMessages();
        }
    };

    componentDidMount = () => {
        this.getMessage();
        this.interval = setInterval(() => {
            this.getMessage();
        }, 3000);
    };

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (this.props.messages !== prevProps.messages) {
            clearInterval(this.interval);
            this.interval = setInterval(() => {
                this.getMessage();
            }, 3000);
            //const messagesBlockScroll = document.getElementById('messagesBlock');
            //messagesBlockScroll.scrollTop = messagesBlockScroll.scrollHeight;
        }
    };

    render() {
        return (
            <Container>
                <ChatMessages messages={this.props.messages}/>
                <hr style={{background:"gray", height:"5px"}}/>
                <ChatForm addMessage={this.addMessage}
                          valueChanged={this.valueChanged}
                          isFilledFields={this.props.isFilledFields}
                />
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.chat.messages,
        lastPostTime: state.chat.lastPostTime,
        error: state.chat.error,
        loading: state.chat.loading,
        isFilledFields:state.chat.isFilledFields,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getThirtyMessages: () => dispatch(getThirtyMessages()),
        getThirtyTimeMessages: (lastPostTime) => dispatch(getThirtyTimeMessages(lastPostTime)),
        addNewMessage: (message) => dispatch(addNewMessage(message))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);
