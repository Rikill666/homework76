import React, {Component} from 'react';
import {Table} from "reactstrap";
import ChatMessage from "./ChatMessage/ChatMessage";
import './ChatMessages.css';

class ChatMessages extends Component {
    render() {
        return (
            <div className="messagesBlock" id="messagesBlock">
                <Table className="table table-inverse">
                    <thead>
                    <tr>
                        <th>Время</th>
                        <th>Автор</th>
                        <th>Сообщение</th>
                    </tr>
                    </thead>
                    <tbody id="messages">
                    {this.props.messages.map(function (message) {
                        return <ChatMessage key={message.id} message={message}/>
                    })}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Время</th>
                        <th>Автор</th>
                        <th>Сообщение</th>
                    </tr>
                    </tfoot>
                </Table>
            </div>
        );
    }
}

export default ChatMessages;
