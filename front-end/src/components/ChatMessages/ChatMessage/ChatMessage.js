import React, {Component} from 'react';
import Moment from "react-moment";

class ChatMessage extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.message !== this.props.message;
    }
    render() {
        return (
            <tr>
                <td>
                    <Moment format="DD.MM.Y - H:mm:ss">
                        {this.props.message.dateTime}
                    </Moment></td>
                <td> {this.props.message.author}</td>
                <td> {this.props.message.message}</td>
            </tr>
        );
    }
}

export default ChatMessage;
