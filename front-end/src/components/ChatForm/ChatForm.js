import React from 'react';
import {Button, Form, Input} from "reactstrap";
import './ChatForm.css';
const ChatForm = (props) => {
    return (
            <Form onSubmit={(e)=>props.addMessage(e)} className="form-inline mb-3 myForm" id="myForm">
                <div className="form-group">
                    <Input onChange={props.valueChanged}
                           className="form-control mr-2"
                           name="author"
                           type="text"
                           placeholder="Имя"
                    />
                </div>
                <div className="form-group">
                    <Input onChange={props.valueChanged}
                           className="form-control"
                           name="message"
                           type="text"
                           placeholder="Сообщение"
                    />
                </div>
                <Button type="submit" className="btn btn-primary ml-2">Submit</Button>
                {!props.isFilledFields?<h5 style={{color:"red", marginLeft:"15px", marginTop:"10px"}}>Fill in the fields</h5>:null}
            </Form>
    );
};

export default ChatForm;