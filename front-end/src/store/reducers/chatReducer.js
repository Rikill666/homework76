import {
    GET_THIRTY_MESSAGES_ERROR,
    GET_THIRTY_MESSAGES_REQUEST,
    GET_THIRTY_MESSAGES_SUCCESS,
    RECEIVE_THIRTY_TIME_MESSAGES_ERROR,
    RECEIVE_THIRTY_TIME_MESSAGES_REQUEST,
    RECEIVE_THIRTY_TIME_MESSAGES_SUCCESS, SENDING_MESSAGE_ERROR, SENDING_MESSAGE_REQUEST, SENDING_MESSAGE_SUCCESS
} from "../actions/chatActions";

const initialState = {
    messages: [],
    loading:false,
    error: null,
    lastPostTime:"",
    isFilledFields: true
};

const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_THIRTY_MESSAGES_REQUEST:
            return {...state, loading: true};
        case GET_THIRTY_MESSAGES_SUCCESS:
            return {...state, messages: action.messages, error:null, lastPostTime:action.lastPostTime, loading: false};
        case GET_THIRTY_MESSAGES_ERROR:
            return {...state,error:action.error, loading: true};

        case RECEIVE_THIRTY_TIME_MESSAGES_REQUEST:
            return {...state, loading: true};
        case RECEIVE_THIRTY_TIME_MESSAGES_SUCCESS:
            const oldMessages = [...state.messages];
            const messages = action.messages.concat(oldMessages);
            return {...state, messages: messages, error:null, lastPostTime:action.lastPostTime, loading: false};
        case RECEIVE_THIRTY_TIME_MESSAGES_ERROR:
            return {...state,error:action.error, loading: true};

        case SENDING_MESSAGE_REQUEST:
            return {...state};
        case SENDING_MESSAGE_SUCCESS:
            return {...state, isFilledFields: true};
        case SENDING_MESSAGE_ERROR:
            return {...state, isFilledFields: false};

        default:
            return state;
    }
};

export default chatReducer;