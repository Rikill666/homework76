import axiosApi from "../../axiosApi";

export const SENDING_MESSAGE_REQUEST = "SENDING_MESSAGE_REQUEST";
export const SENDING_MESSAGE_SUCCESS = "SENDING_MESSAGE_SUCCESS";
export const SENDING_MESSAGE_ERROR = "SENDING_MESSAGE_ERROR";

export const GET_THIRTY_MESSAGES_REQUEST = "GET_THIRTY_MESSAGES_REQUEST";
export const GET_THIRTY_MESSAGES_SUCCESS = "GET_THIRTY_MESSAGES_SUCCESS";
export const GET_THIRTY_MESSAGES_ERROR = "GET_THIRTY_MESSAGES_ERROR";

export const RECEIVE_THIRTY_TIME_MESSAGES_REQUEST = "RECEIVE_THIRTY_TIME_MESSAGES_REQUEST";
export const RECEIVE_THIRTY_TIME_MESSAGES_SUCCESS = "RECEIVE_THIRTY_TIME_MESSAGES_SUCCESS";
export const RECEIVE_THIRTY_TIME_MESSAGES_ERROR = "RECEIVE_THIRTY_TIME_MESSAGES_ERROR";

export const thirtyMessagesRequest = () => {
    return {type: GET_THIRTY_MESSAGES_REQUEST};
};

export const thirtyMessagesSuccess = (messages,lastPostTime) => {
    return {type: GET_THIRTY_MESSAGES_SUCCESS, messages, lastPostTime};
};

export const thirtyMessagesError = (error) => {
    return {type: GET_THIRTY_MESSAGES_ERROR, error};
};

export const getThirtyMessages = () => {
    return async dispatch => {
        try{
            dispatch(thirtyMessagesRequest());
            const response = await axiosApi.get('/messages');
            const messages = response.data;
            if(messages.length > 0) {
                const lastPostTime = messages[0].dateTime;
                dispatch(thirtyMessagesSuccess(messages, lastPostTime));
            }
        }
        catch (e) {
            dispatch(thirtyMessagesError(e));
        }
    };
};

export const thirtyTimeMessagesRequest = () => {
    return {type: RECEIVE_THIRTY_TIME_MESSAGES_REQUEST};
};

export const thirtyTimeMessagesSuccess = (messages, lastPostTime) => {
    return {type: RECEIVE_THIRTY_TIME_MESSAGES_SUCCESS, messages, lastPostTime};
};

export const thirtyTimeMessagesError = (error) => {
    return {type: RECEIVE_THIRTY_TIME_MESSAGES_ERROR, error};
};

export const getThirtyTimeMessages = (lastPostTime) => {
    return async dispatch=> {
        try{
            dispatch(thirtyTimeMessagesRequest());
            const response = await axiosApi.get('/messages?datetime=' + lastPostTime);
            const messages = response.data;
            if(messages.length > 0){
                const lastPostTime = messages[0].dateTime;
                dispatch(thirtyTimeMessagesSuccess(messages, lastPostTime));
            }
        }
        catch (e) {
            dispatch(thirtyTimeMessagesError(e));
        }
    };
};
export const sendMessageRequest = () => {
    return {type: SENDING_MESSAGE_REQUEST};
};

export const sendMessageSuccess = () => {
    return {type: SENDING_MESSAGE_SUCCESS};
};

export const sendMessageError = (error) => {
    return {type: SENDING_MESSAGE_ERROR, error};
};

export const addNewMessage = (message) => {
    return async dispatch => {
        try{
            dispatch(sendMessageRequest());
            await axiosApi.post('/messages', message);
            dispatch(sendMessageSuccess());
        }
        catch (e) {
            dispatch(sendMessageError(e));
        }
    };
};