const express = require('express');
const fileDb = require("../fileDb");

const router = express.Router();



router.get('/', async (req, res) => {
    if(req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())){
            res.status(400).send({error: "Date is incorrect"});
        }
        else{
            let messages = await fileDb.getMessages();
            messages = messages.filter(m => new Date(m.dateTime) > new Date(req.query.datetime)).sort((a, b) => {
                return new Date(b.dateTime) - new Date(a.dateTime);
            });
            res.send(messages);
        }
    } else {
        let messages = await fileDb.getMessages();
        messages = messages.sort((a, b) => {
            return new Date(b.dateTime) - new Date(a.dateTime);
        }).slice(0, 30);
        res.send(messages);
    }
});

router.post('/', async (req, res) => {
    if(req.body.author && req.body.message){
        await fileDb.addMessage(req.body);
        res.send(req.body);
    }
    else{
        res.status(400).send({error: "Author and message must be present in the request"});
    }
});

module.exports = router;